module.exports = [
    "./tasks/clean",
    "./tasks/twig",
    "./tasks/styles",
    "./tasks/scripts",
    "./tasks/images",
    "./tasks/watch",
    "./tasks/serve",
    "./tasks/server_conf"
];