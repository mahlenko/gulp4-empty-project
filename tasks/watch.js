module.exports = function() {
    $.gulp.task("watch", function() {
        return new Promise((res, rej) => {
            $.watch(["./src/views/**/index.twig", "!./src/views/partials/*.twig"], $.gulp.series("twig"));
            $.watch("./src/styles/**/main.scss", $.gulp.series("styles"));
            $.watch(["./src/images/**/*.{jpg,jpeg,png,gif}", "!./src/images/icons/svg/*.svg", "!./src/images/favicons/*.{jpg,jpeg,png,gif}"], $.gulp.series("images"));
            $.watch("./src/scripts/**/*.js", $.gulp.series("scripts"));
            res();
        });
    });
};