module.exports = function() {
    $.gulp.task("twig", function () {
        return $.gulp.src(['./src/views/*.twig', '!./src/views/partials/*.twig'])
            .pipe($.twig({
                data: {},
                functions: [
                    {name: 'site_url',   func: function(string = '') { return '/' + string; }},
                    {name: 'assets',     func: function(string = '') { return '/' + string; }},
                    /* CodeIgniter */
                    {name: 'form_open',  func: function() { return '<form action="/" method="post">'; }},
                    {name: 'form_close', func: function() { return '</form>'; }}
                ],
                filters: []
            }))
            .pipe($.gulp.dest('./dest/'))
            .pipe($.debug({"title": "twig"}))
            .on("end", $.browsersync.reload);
    });
};